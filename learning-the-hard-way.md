# Learning the hard way how pre-commit hooks work

I'm not an expert on Git hooks. I've hardly ever used them. If you want to use a pre-commit hook, it only runs if there's something to commit. I couldn't figure out why it didn't work. Then I made a change to a file, and it *did* work.

Okay.
