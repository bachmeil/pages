# Using a pre-commit hook to build this site with Bash

I convert the markdown files in this repo into a site, including generation of an index, automagically using a Bash script. That might seem dumb, but

- Building pages is extremely simple. Pages are built by concatenating the markdown into an html file, with markdown-it converting the markdown to html in the browser. This gives me full control over the layout.
- Bash plus Unix utilities is a pretty good toolbox for something like this. It's quite easy to generate the index. I loop over the files, have sed grab the first line (a header), then have cut drop the first two characters. That is the description, which is appended as part of a link to the index page.
- Git hooks are just regular scripts that run on your computer. I add the newly generated html files from within the script. Updated html files are committed after running the script.
- You could use this as a blog if you wanted. Start the name of the markdown file with the date, and everything will be ordered. If you want, you can add the date to the index, or even to the generated html file. It would not take much.
- I have complete control over the full generator. Using someone else's work means I have to figure out how to fiddle with their system, and that's rarely fun.
