# Index of Pages

- [Using a pre-commit hook to build this site with Bash](building-this-site.html)
- [Learning the hard way how pre-commit hooks work](learning-the-hard-way.html)
- [Trying out Codeberg](trying-out.html)
